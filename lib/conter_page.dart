
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:fluttermdr/data/storeTest/counter.dart';
import 'package:provider/provider.dart';

class ConterPage extends StatelessWidget {
  ConterPage({Key key});

  @override
  Widget build(BuildContext context) {
    final counter = Provider.of<Counter>(context);
    return Card(
      color: Colors.lime,
      elevation: 20,
      margin: EdgeInsets.all(100),
      child: Column(
        children: [
          Observer(builder: (_){
            return Text(counter.value.toString());
          }),
          FlatButton.icon(
              onPressed: () {
                counter.acroit();
              },
              icon: Icon(Icons.add),
              label: Text('Add')),
          FlatButton.icon(
              onPressed: () {
                counter.decroit();
              },
              icon: Icon(Icons.close),
              label: Text('Moin')),
        ],
      ),
    );
  }
}
