import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttermdr/data/dataSources/firestoreDataSource/firestore.dart';
import 'package:fluttermdr/data/dataSources/localDataSource/characteur_hive_provider.dart';
import 'package:fluttermdr/data/dataSources/remoteDataSource/dio_helper.dart';
import 'package:fluttermdr/data/entities/response.dart';

class LoginPage extends StatefulWidget {
  static const String routeName = '/login';

  const LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String text;
  String result = 'data';
  CharacterHiveProvider _provider;

  Future<void> _insertHiver(Results object) async {
    _provider ??= await CharacterHiveProvider.create();
    return _provider.add(object.name, object);
  }

  @override
  Widget build(BuildContext context) {
    final argument = ModalRoute.of(context).settings.arguments;
    if (argument != null && argument is Map) {
      if (argument.containsKey('exemple')) {
        text = argument['exemple'];
      }
    }
    return Scaffold(
        appBar: AppBar(
          title: Text('Login Page'),
        ),
        body: Center(
            child: FutureBuilder(
          future: DioHelper.instance.get('character/'),
          builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              if (snapshot.hasData) {
                final test = Response.fromJson(snapshot.data);
                final char = test.results[0];
                _insertHiver(char);
                Firestore.instance
                    .insertDocument('character', char.toJson())
                    .then((value) {
                  if (result != 'Done') {
                    setState(() {
                      result = 'Done';
                    });
                  }
                });

                return Center(
                  child: Text(result),
                );
              } else {
                return Text('C est vide');
              }

              // return Text(test.results[0].name + "\n" + test.results[1].name);

            } else {
              return CircularProgressIndicator();
            }
          },
        )));
  }
}
