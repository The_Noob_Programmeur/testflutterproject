import 'package:cloud_firestore/cloud_firestore.dart';

class Firestore {
  FirebaseFirestore firestore;

  Firestore._privateConstructor() {
    firestore ??= FirebaseFirestore.instance;
  }

  static final Firestore _instance = Firestore._privateConstructor();

  static Firestore get instance => _instance;

  CollectionReference getCollection(String collectionName) {
    return firestore.collection(collectionName);
  }

  Future<int> insertDocument(
      String collectionName, Map<String, dynamic> object) async {
    await getCollection(collectionName).doc().set(object);
    return 1;
  }

  Future<DocumentSnapshot> getDocumentById(String collectionName, String id) {
    getCollection(collectionName).doc(id).get();
  }

  Future<void> deleteDocumentById(String collectionName, String id) {
    getCollection(collectionName).doc(id).delete();
  }
}
