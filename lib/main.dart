import 'package:flutter/material.dart';
import 'package:fluttermdr/conter_page.dart';
import 'package:fluttermdr/login_page.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:provider/provider.dart';

import 'data/storeTest/counter.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider<Counter>(
          create: (_) => Counter(),
        )
      ],
      child: MaterialApp(
          title: 'Flutter Demo',
          theme: ThemeData(
            primarySwatch: Colors.green,
            visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          routes: <String, WidgetBuilder>{
            LoginPage.routeName: (BuildContext context) => const LoginPage()
          },
          home: Consumer<Counter>(builder: (
              context,
              counter,
              _){
          return ConterPage();
          })
        // MyHomePage(title: 'Premiére Application de Antoine Berthier'),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {


  @override
  Widget build(BuildContext context) =>
      Scaffold(
        body: Center(
          child: IconButton(
            icon: Icon(Icons.wifi),
            onPressed: () {
              Navigator.pushNamed(context, LoginPage.routeName,
                  arguments: {'exemple', 'Je suis un exemple'});
            },
          ),
        ),
      );
}
